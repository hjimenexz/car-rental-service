package biz;

import app.err.*;
import dao.CarReservationDao;
import dto.CarAvailability;
import dto.CarModel;
import dto.CarType;
import dto.Reservation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CarReservationBizTest {
    CarReservationBiz carReservationBiz;
    CarReservationDao carReservationDaoMock;

    @Before
    public void setUp() throws Exception {
        carReservationBiz = new CarReservationBiz();
        carReservationDaoMock = mock(CarReservationDao.class);
        carReservationBiz.setDao(carReservationDaoMock);
    }


    @Test
    public void createReservationTest()throws BusinessException{
        Reservation sampleSuccessfulReservation = new Reservation();
        sampleSuccessfulReservation.setClientDetails("SampleClientDetails");
        when(carReservationDaoMock.createCarReservation(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(sampleSuccessfulReservation);

        Reservation createdReservation = carReservationBiz.createReservation("11/01/2018 00:00", "11/01/2018 22:00", CarType.SEDAN.toString(),
                CarModel.FOCUS.toString(), "ClientDetails", "ClientPayment");

        Assert.assertEquals(createdReservation.getClientDetails(), "SampleClientDetails");
    }

    @Test (expected = PriceExpiredException.class)
    public void createReservationPriceExpiredTest()throws BusinessException {
        Reservation sampleSuccessfulReservation = new Reservation();
        sampleSuccessfulReservation.setClientDetails("SampleClientDetails");
        when(carReservationDaoMock.createCarReservation(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenThrow(new PriceExpiredException());

        carReservationBiz.createReservation("11/01/2018 00:00", "11/01/2018 22:00", CarType.SEDAN.toString(),
                CarModel.FOCUS.toString(), "ClientDetails", "ClientPayment");

    }

    @Test (expected = DBErrorException.class)
    public void createReservationDBerrorTest()throws BusinessException {
        Reservation sampleSuccessfulReservation = new Reservation();
        sampleSuccessfulReservation.setClientDetails("SampleClientDetails");
        when(carReservationDaoMock.createCarReservation(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenThrow(new DBErrorException());

        carReservationBiz.createReservation("11/01/2018 00:00", "11/01/2018 22:00", CarType.SEDAN.toString(),
                CarModel.FOCUS.toString(), "ClientDetails", "ClientPayment");

    }

    @Test (expected = CarNoLongerAvailableException.class)
    public void createReservationCarNoLongerAvailableTest()throws BusinessException{
        Reservation sampleSuccessfulReservation = new Reservation();
        sampleSuccessfulReservation.setClientDetails("SampleClientDetails");
        when(carReservationDaoMock.createCarReservation(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenThrow(new CarNoLongerAvailableException());

        carReservationBiz.createReservation("11/01/2018 00:00", "11/01/2018 22:00", CarType.SEDAN.toString(),
                CarModel.FOCUS.toString(), "ClientDetails", "ClientPayment");
    }

    @Test
    public void createMultiReservationTest()throws BusinessException{
        Reservation sampleSuccessfulReservation = new Reservation();
        sampleSuccessfulReservation.setClientDetails("SampleClientDetails");
        when(carReservationDaoMock.createMultiDayCarReservation(Mockito.any(), Mockito.any(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(sampleSuccessfulReservation);

        OffsetDateTime[] fromDates =
                { OffsetDateTime.parse("2018-12-03T10:30+01:00"), OffsetDateTime.parse("2018-12-13T10:30+01:00")};
        OffsetDateTime[] toDates =
                { OffsetDateTime.parse("2018-12-05T10:30+01:00"), OffsetDateTime.parse("2018-12-17T10:30+01:00") };

        Reservation createdReservation = carReservationBiz.createMultiDayReservation(fromDates, toDates, CarType.SEDAN.toString(),
                CarModel.FOCUS.toString(), "ClientDetails", "ClientPayment");

        Assert.assertEquals(createdReservation.getClientDetails(), "SampleClientDetails");
    }

    @Test(expected = OverlappingDatesException.class)
    public void createOverlappingMultiReservationTest()throws BusinessException{
        Reservation sampleSuccessfulReservation = new Reservation();
        sampleSuccessfulReservation.setClientDetails("SampleClientDetails");
        when(carReservationDaoMock.createMultiDayCarReservation(Mockito.any(), Mockito.any(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(sampleSuccessfulReservation);

        OffsetDateTime[] fromDates =
                { OffsetDateTime.parse("2018-12-03T10:30+01:00"), OffsetDateTime.parse("2018-12-04T10:30+01:00")};
        OffsetDateTime[] toDates =
                { OffsetDateTime.parse("2018-12-05T10:30+01:00"), OffsetDateTime.parse("2018-12-17T10:30+01:00") };

        Reservation createdReservation = carReservationBiz.createMultiDayReservation(fromDates, toDates, CarType.SEDAN.toString(),
                CarModel.FOCUS.toString(), "ClientDetails", "ClientPayment");

        Assert.assertEquals(createdReservation.getClientDetails(), "SampleClientDetails");
    }

    @Test
    public void queryCarAvailabilityTest()throws DBErrorException{
        CarAvailability cars[] = {new CarAvailability(CarType.SUV, CarModel.ECOSPORT, "11/01/2018", "00:00", 1.5),
                new CarAvailability(CarType.CLASSIC, CarModel.MODELT, "11/01/2018", "00:00", 1.5)};
        List<CarAvailability> carsList = Arrays.asList(cars);

        when(carReservationDaoMock.queryCarAvailability(Mockito.anyString(), Mockito.anyString())).thenReturn(carsList);

        carsList = carReservationBiz.queryCarAvailability("12/01/2018 00:00", "12/03/2018 00:00");

        Assert.assertEquals(carsList.get(0).getCarModel(), CarModel.ECOSPORT);
    }

}
