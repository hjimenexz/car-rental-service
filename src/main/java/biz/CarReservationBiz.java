package biz;

import app.err.CarNoLongerAvailableException;
import app.err.DBErrorException;
import app.err.OverlappingDatesException;
import app.err.PriceExpiredException;
import dao.CarReservationDao;
import dto.CarAvailability;
import dto.Reservation;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * Executes business logic and interacts with reservation DB.
 *
 * Throws UnsupportedOperationException since it will be mocked for JUnit tests for the assignment.
 * Otherwise it would call an actual DB instead but it's not implemented for the assignment.
 */
public class CarReservationBiz implements CarReservation {
    CarReservationDao dao;

    @Override
    public List<CarAvailability> queryCarAvailability(String fromDate, String toDate) throws DBErrorException {
        return dao.queryCarAvailability(fromDate, toDate);
    }

    @Override
    public Reservation getReservation(String reservationId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Reservation> findReservation(String clientDetails) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Reservation createReservation(String fromDate, String toDate, String carType, String carModel, String clientDetails, String clientPayment)
    throws DBErrorException, PriceExpiredException, CarNoLongerAvailableException {
        return dao.createCarReservation(fromDate, toDate, carType, carModel, clientDetails, clientPayment);
    }

    @Override
    public Reservation createMultiDayReservation(OffsetDateTime[] fromDate,
                                                 OffsetDateTime[] toDate, String carType, String carModel, String clientDetails, String clientPayment)
            throws DBErrorException, PriceExpiredException, CarNoLongerAvailableException, OverlappingDatesException {

        if(overlappingTimes(fromDate, toDate)){
            throw new OverlappingDatesException();
        }

        return dao.createMultiDayCarReservation(fromDate, toDate, carType, carModel, clientDetails, clientPayment);
    }

    /**
     * Compares each toDate with the following fromDate, returning true if the times overlap.
     *
     * Assumes that fromDate and toDate have the same length and they come ordered as well.
     *
     * @param fromDate
     * @param toDate
     * @return
     */
    boolean overlappingTimes(OffsetDateTime[] fromDate, OffsetDateTime[] toDate) {
        assert(fromDate != null && toDate != null && fromDate.length == toDate.length);

        for(int i = 0; i < fromDate.length; i++){
            if (i+1 < toDate.length){
//                if(toDate[i].compareTo( fromDate[i+1] ) > 0){
//                    return true;
//                }
                if(toDate[i].isAfter(fromDate[i+1])){
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public Reservation deleteReservation(String reservationId) {
        throw new UnsupportedOperationException();
    }

    public CarReservationDao getDao() {
        return dao;
    }

    public void setDao(CarReservationDao dao) {
        this.dao = dao;
    }
}
