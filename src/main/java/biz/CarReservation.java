package biz;

import app.err.CarNoLongerAvailableException;
import app.err.DBErrorException;
import app.err.OverlappingDatesException;
import app.err.PriceExpiredException;
import dto.CarAvailability;
import dto.Reservation;

import java.time.OffsetDateTime;
import java.util.List;

public interface CarReservation {
    List<CarAvailability> queryCarAvailability(
            String fromDate,
            String toDate
    ) throws DBErrorException;

    Reservation getReservation(String reservationId) throws DBErrorException;

    List<Reservation> findReservation(String clientDetails)
            throws DBErrorException;

    Reservation createReservation(String fromDate,
                                  String toDate,
                                  String carType,
                                  String carModel,
                                  String clientDetails,
                                  String clientPayment)
            throws DBErrorException, CarNoLongerAvailableException, PriceExpiredException;

    Reservation createMultiDayReservation(OffsetDateTime[] fromDate,
                                          OffsetDateTime[] toDate,
                                          String carType,
                                          String carModel,
                                          String clientDetails,
                                          String clientPayment)
            throws DBErrorException, CarNoLongerAvailableException, PriceExpiredException, OverlappingDatesException;

    Reservation deleteReservation(String reservationId) throws DBErrorException;
}
