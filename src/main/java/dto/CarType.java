package dto;

public enum CarType {
    SEDAN, CLASSIC, SUV, SPORT, COMPACT;
}
