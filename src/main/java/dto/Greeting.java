package dto;

/**
 * This class is just used to verify that the Spring Boot container launched ok, it's not part of the assignment.
 */
public class Greeting {

    private final long id;
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
