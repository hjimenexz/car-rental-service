package dto;

/**
 * A reservation is based upon a car being available, so extended from this class to get the same attributes.
 */
public class Reservation extends CarAvailability{
    private String clientDetails;
    private String paymentDetails;

    public String getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(String clientDetails) {
        this.clientDetails = clientDetails;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
