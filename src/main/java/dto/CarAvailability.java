package dto;

public class CarAvailability {
    private CarType carType;
    private CarModel carModel;
    private String date;
    private String hour;
    private double price;

    public CarAvailability(){}

    public CarAvailability(CarType carType, CarModel carModel, String date, String hour, double price){
        this.carType = carType;
        this.carModel = carModel;
        this.date = date;
        this.hour = hour;
        this.price = price;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
