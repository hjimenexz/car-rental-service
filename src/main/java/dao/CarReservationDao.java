package dao;

import app.err.CarNoLongerAvailableException;
import app.err.DBErrorException;
import app.err.PriceExpiredException;
import dto.CarAvailability;
import dto.Reservation;

import java.time.OffsetDateTime;
import java.util.List;

public class CarReservationDao {
    public Reservation createCarReservation(String fromDate, String toDate, String carType, String carModel, String clientDetails, String clientPayment)
    throws PriceExpiredException, CarNoLongerAvailableException, DBErrorException {
        throw new UnsupportedOperationException();
    }

    public Reservation createMultiDayCarReservation(OffsetDateTime[] fromDate,
                                                    OffsetDateTime[] toDate, String carType, String carModel, String clientDetails, String clientPayment)
            throws PriceExpiredException, CarNoLongerAvailableException, DBErrorException {
        throw new UnsupportedOperationException();
    }

    public List<CarAvailability> queryCarAvailability(String fromDate, String toDate) throws DBErrorException{
        throw new UnsupportedOperationException();
    }
}
