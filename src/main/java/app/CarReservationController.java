package app;

import app.err.CarNoLongerAvailableException;
import app.err.DBErrorException;
import app.err.PriceExpiredException;
import biz.CarReservation;
import dto.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class CarReservationController {
    private CarReservation carReservationBiz;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    /**
     * This method is just used to verify that the Spring Boot container launched ok, it's not part of the assignment.
     * @param name
     * @return
     */
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }

    @GetMapping("/queryAvailability")
    public List<CarAvailability> queryCarAvailability(
            @RequestParam(value = "from") String fromDate,
            @RequestParam(value = "to") String toDate
    ){
        if(fromDate == null || toDate == null){
            throw new IllegalArgumentException("From and to date are required.");
        }

        try {
            return carReservationBiz.queryCarAvailability(fromDate, toDate);
        } catch (DBErrorException e) {
            // TODO Handle exception
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/getReservation")
    public Reservation getReservation(@RequestParam(value = "reservationId") String reservationId){
        if(reservationId == null){
            throw new IllegalArgumentException("Reservation id is required");
        }

        try {
            return carReservationBiz.getReservation(reservationId);
        } catch (DBErrorException e) {
            // TODO Handle exception
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/findReservation")
    public List<Reservation> findReservation(@RequestParam(value = "clientDetails") String clientDetails){
        if(clientDetails == null){
            throw new IllegalArgumentException("ClientDetails are required");
        }

        try {
            return carReservationBiz.findReservation(clientDetails);
        } catch (DBErrorException e) {
            // TODO Handle exception
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/createReservation")
    public Reservation createReservation(@RequestParam(value = "from") String fromDate,
                                         @RequestParam(value = "to") String toDate,
                                         @RequestParam(value = "carType") String carType,
                                         @RequestParam(value = "carModel") String carModel,
                                         @RequestParam(value = "clientDetails") String clientDetails,
                                         @RequestParam(value = "clientPayment") String clientPayment){
        if(fromDate == null || toDate == null || carType == null || carModel == null || clientDetails == null || clientPayment == null ){
            throw new IllegalArgumentException("Required parameter not present.");
        }

        try {
            return carReservationBiz.createReservation(fromDate, toDate, carType, carModel, clientDetails, clientPayment);
        } catch (DBErrorException e) {
            // TODO Handle exceptions
            e.printStackTrace();
        } catch (CarNoLongerAvailableException carNoLongerAvailable) {
            carNoLongerAvailable.printStackTrace();
        } catch (PriceExpiredException e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping("/deleteReservation")
    public Reservation deleteReservation(@RequestParam(value = "reservationId") String reservationId){
        if(reservationId == null){
            throw new IllegalArgumentException("Reservation id is required");
        }

        try {
            return carReservationBiz.deleteReservation(reservationId);
        } catch (DBErrorException e) {
            // TODO Handle exception
            e.printStackTrace();
        }
        return null;
    }


    /* Method for assigning a CarReservation business layer object via dependency injection: */

    public void setCarReservationBiz(CarReservation carReservationBiz) {
        this.carReservationBiz = carReservationBiz;
    }

    public CarReservation getCarReservationBiz() {
        return carReservationBiz;
    }
}
