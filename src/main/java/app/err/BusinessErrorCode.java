package app.err;

/**
 * Used for exceptions in the business layer.
 *
 * <ul>
 * <li>DB_ERROR: Generic DB error including communication errors.</li>
 * <li>NO_CAR_AVAILABLE: Car is no longer available at the time of reservation commit
 *   (someone else took it for a requested time period).</li>
 * <li>PRICE_EXPIRED: The quoted price expired while making the reservation.</li>
 * <li>OVERLAPPING_DATES: The dates for the reservation are overlapping</li>
 * </ul>
 */
public enum BusinessErrorCode {
    DB_ERROR, NO_CAR_AVAILABLE, PRICE_EXPIRED, OVERLAPPING_DATES;
}
