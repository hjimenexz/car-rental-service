package app.err;

public class BusinessException extends Exception {
    private BusinessErrorCode businessErrorCode;

    public BusinessException(BusinessErrorCode businessErrorCode){
        super();
        this.businessErrorCode = businessErrorCode;
    }

    /* TODO implement other standard constructors taking message and throwable parameters, not required for homework. */
}
