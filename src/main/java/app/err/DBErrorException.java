package app.err;

public class DBErrorException extends BusinessException {
    public DBErrorException(){
        super(BusinessErrorCode.DB_ERROR);
    }
}
