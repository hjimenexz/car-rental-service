package app.err;

public class OverlappingDatesException extends BusinessException {
    public OverlappingDatesException(){
        super(BusinessErrorCode.OVERLAPPING_DATES);
    }
}
